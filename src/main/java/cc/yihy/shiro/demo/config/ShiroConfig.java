package cc.yihy.shiro.demo.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import cc.yihy.shiro.demo.realm.UserRealm;
import cc.yihy.shiro.demo.repositories.UserRepository;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Package: cc.yihy.shiro.demo.config
 * @date: 2017/8/28 23:21
 * @author: Yihy
 * @version: V1.0
 * @Description:
 */
@Configuration
public class ShiroConfig {

    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }

    /**
     * 验证用户
     * 可以配置多个Realm Bean，Shiro都会把它注入的
     *
     * @return
     */
    @Bean
    @Autowired
    public Realm realm(UserRepository userRepository) {
        UserRealm userRealm = new UserRealm();
        userRealm.setUserRepository(userRepository);
        return userRealm;
    }

    /**
     * 配置shiro的url权限
     *
     * @return
     */
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
        chainDefinition.addPathDefinition("/login.html", "authc"); // need to accept POSTs from the login form
        chainDefinition.addPathDefinition("/logout", "logout");
        chainDefinition.addPathDefinition("/account-info", "perms[write]");
        chainDefinition.addPathDefinition("/account-info1", "roles[admin]");

        return chainDefinition;
    }


//    @Override
//    protected ShiroFilterFactoryBean shiroFilterFactoryBean() {
//        ShiroFilterFactoryBean factoryBean = super.shiroFilterFactoryBean();
//        Map<String,Filter> filterMap = new LinkedHashMap<>();
//        filterMap.put("anyrole",new UserFilter());
//        factoryBean.setFilters(filterMap);
//        return factoryBean;
//    }
}
