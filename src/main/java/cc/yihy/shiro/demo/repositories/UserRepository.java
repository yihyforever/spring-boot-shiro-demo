package cc.yihy.shiro.demo.repositories;


import cc.yihy.shiro.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username);
}